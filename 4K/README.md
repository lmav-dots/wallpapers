0-7. Wallpapers based off the short Afterlives: Ardenweald
  * https://www.wowhead.com/news=317934/afterlives-ardenweald-4k-desktop-wallpapers

8. "One More Time" a Daft Punk themed wallpaper, made after the announcement of them ending their music carrer.
  * https://www.reddit.com/r/wallpaper/comments/lqmuyg/

9. Untitled Abstract Wallpaper, a wallpaper made by reddit user u/HurtTree, and posted to r/wallpaper
  * https://drive.google.com/file/d/1dnEuK1we1ory2c1_wtc1jkaki4lUqq6q/view